import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    tasks: [
      {
        id: 1,
        name: "牛乳を買う",
        labelIds: [1, 2],
        done: false
      },
      {
        id: 2,
        name: "Vue.jsの本を買う",
        labelIds: [1, 3],
        done: true
      }
    ],
    labels: [
      {
        id: 1,
        text: "買い物"
      },
      {
        id: 2,
        text: "食料"
      },
      {
        id: 3,
        text: "本"
      }
    ],
    nextTaskId: 3,
    nextlabelId: 4,

    filter: null
  },

  getters: {
    filteredTasks(state) {
      if (!state.filter) {
        return state.tasks;
      }

      return state.tasks.filter(task => {
        return task.labelIds.indexOf(state.filter) >= 0;
      });
    }
  },

  mutations: {
    // タスク追加するやつ
    addTask(state, { name, labelIds }) {
      state.tasks.push({
        id: state.nextTaskId,
        name,
        labelIds,
        done: false
      });

      // 次のタスクidを加算
      state.nextTaskId++;
    },

    // タスクの完了状態を更新するやつ
    // チェックボックスの変更で発火
    toggleTaskStatus(state, { id }) {
      // filterdにはクリックされたタスクが1つだけ入る
      const filtered = state.tasks.filter(task => {
        return task.id === id;
      });

      filtered.forEach(task => {
        // toggleの操作
        task.done = !task.done;
        // task.name = filtered[0].name;
        // task.name = filtered;
      });
    },

    // ラベル追加
    addLabel(state, { text }) {
      state.labels.push({
        id: state.nextlabelId,
        text
      });
      state.nextlabelId++;
    },

    changeFilter(state, { filter }) {
      state.filter = filter;
    }
  }
});

export default store;
